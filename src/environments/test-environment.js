'use strict';

const ModelEnvironment = require('diary-core/model_environment'),
      Serializers = require('diary-core/model_serializers'),
      TestStore = require('diary-core/test_store')

const Diary = require('diary-core/diary')

class Environment extends ModelEnvironment {
  constructor() {
    super()
    this.store = new TestStore()
    this.serializer = new Serializers.Dummy()
    // A user should have a diary.
    // const diary = this.model(Diary).fromAttributes({name: 'Demo', localId: '1'})
    // this.store.add(diary)

    // We want encrypted diary.name, but crypto is not yet initialized, so
    // add directly to the store.
    const diaryAttrs = {
      id: 1,
      localId: '1',
      name: '{"encrypted":"DOF9FTu+EYmz3X9REMDzzQ==",' +
        '"iv":"nvRGwbkr0wqn7enjOWw4dQ=="}'
    }
    this.store.pages.diaries = {1: diaryAttrs}
  }
}

module.exports = Environment
