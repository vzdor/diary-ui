"use strict";

import riot from "riot"
// Make riot globally visible
window.riot = riot
import route from "riot-route"
window.route = route

// Css
import './app.css'

// The entry tag
import './app.tag'
