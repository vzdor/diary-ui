'use strict';

const Errors = require('diary-core/errors')

// Validates the model and provides feedback to the view on the
// save progress.
class Form {
  // A view should provide methods:
  //   trigger(name, args)
  //   update(options)
  constructor(view, model) {
    this.view = view
    this.model = model
  }

  async save({validate = true} = {}) {
    if (validate) this.validate()

    if (this.anyErrors()) return

    this.view.update({saving: true, message: 'Saving...'})
    try {
      await this.saveModel()
      if (this.anyErrors())
        this.view.update({saving: false, message: null})
      else
        await this.saved()
    } catch (error) {
      console.error(error)
      // Display user-friendly error.
      const message = Errors.errorMessage(error)
      this.view.update({saving: false, message})
    }
  }

  validate() {

  }

  anyErrors() {
    return Object.keys(this.model.errors).length > 0
  }

  saveModel() {
    return this.model.save()
  }

  async saved() {
    this.view.trigger('saved')
  }
}

module.exports = Form
