<entry-form>
    <form onsubmit="{save}">
        <div class="c-tabs">
            <div class="c-tabs__headings">
                <div class="{c-tab-heading: true, c-tab-heading--active: editorTab}" onclick="{edit}">Editor</div>
                <div class="{c-tab-heading: true, c-tab-heading--active: !editorTab}" onclick="{preview}">Output</div>
            </div>
            <div class="{c-tabs__tab: true, c-tabs__tab--active: editorTab}">
                <textarea ref="message" class="{c-field: true, c-field--error: entry.errors.message}" value="{entry.message}" required></textarea>
                <diary-form-field-errors errors="{entry.errors.message}" />
            </div>
            <div class="{preview-tab: true, c-tabs__tab: true, c-tabs__tab--active: !editorTab}">
                <diary-markup text="{refs.message.value}"></diary-markup>
            </div>
        </div>
        <div class="o-form-element">
            <a href="#/">Cancel</a>
            <button class="c-button c-button--success" disabled="{saving}" type="submit">Save</button>
            <span if="{message}">{message}</span>
        </div>
    </form>

    <script>
     import './form-errors.tag'
     import './markup.tag'

     import Form from './forms/entry-form'

     const form = new Form({
         view: this,
         model: opts.entry,
         store: opts.store
     })

     this.entry = form.model
     this.editorTab = true

     const serialize = () => {
         this.entry.message = this.refs.message.value
     }

     save(e) {
         e.preventDefault()
         serialize()
         form.save()
     }

     edit(e) {
         this.editorTab = true
     }

     preview(e) {
         this.editorTab = false
         // this.tags['diary-markup'].update()
     }

     this.on('saved', () => route('/filter'))

     const focus = () => this.refs.message.focus()

     this.on('updated', focus)
     this.on('mount', focus)
    </script>

    <style>
     :scope [ref="message"] {
         height: 10em;
     }
     :scope .c-tabs__tab {
         min-height: 12em;
         padding: 1em 0;
     }
    </style>
</entry-form>
