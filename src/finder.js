'use strict';

const {PagedFinder, PagedTextFinder} = require('diary-core/paged_finders'),
      EntryModel = require('diary-core/entry'),
      urls = require('./urls')

class Finder {
  constructor({env}) {
    this.env = env
    this.hideListing = env.diary.hideListing
    // The model used by finders.
    this.Model = env.model(EntryModel)
    // Previous query options.
    this.options = {}
  }

  url(opts = {}) {
    let copy = Object.assign({}, this.options, opts)
    return urls.filterUrl(copy)
  }

  capture({label} = {}) {
    let query = route.query(),
          options = {
            label: label,
            page: parseInt(query.page || 0)
          }
    if (query.q) options.text = decodeURIComponent(query.q)
    this._setFinder(this.options, options)
    this.options = options
  }

  // Use the same finder if text nor label did not change.
  _setFinder(prevOptions, options) {
    if (prevOptions.text == options.text &&
        prevOptions.label == options.label && this.finder) return
    if (options.text)
      this.finder = new PagedTextFinder({text: options.text, env: this.env})
    else if (this.hideListing)
      this.finder = null
    else
      this.finder = new PagedFinder(this.Model)

  }

  // Returns a promise.
  find() {
    this.models = [] // Clean models.
    if (!this.finder) return Promise.resolve()
    return this.finder.find(this.options.page).then((models) => {
      this.pagesCount = this.finder.pagesCount
      this.models = models
      return models
    })
  }
}

module.exports = Finder
