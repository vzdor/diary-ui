<diary-layout>
    <div class="o-container o-container--xsmall u-window-box--small">
        <div data-is="{opts.component}" store="{opts.store}"></div>
    </div>

    <script>
     // Pages
     import './pages/signup.tag'
     import './pages/signin.tag'
    </script>
</diary-layout>
