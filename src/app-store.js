'use strict';

const riot = require('riot')

const Environment = require('./environment')

const appStore = {
  env: new Environment(),
  get diary() { return this.env.diary },
  isSignedIn: function() { return this.diary !== undefined }
}

riot.observable(appStore)

appStore.on('set-diary', (diary, diaryCrypto) => {
  appStore.env.diary = diary
  appStore.env.diaryCrypto = diaryCrypto
})

appStore.on('add-diary', (newDiary) => {
  appStore.diaries.push(newDiary)
})

appStore.on('sign-in', ({env, diaries}) => {
  appStore.env = env
  appStore.diaries = diaries.sort((d1, d2) => {
    return d1.name.localeCompare(d2.name)
  })

  route('filter')
})

module.exports = appStore
