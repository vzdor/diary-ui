'use strict';

const ModelEnvironment = require('diary-core/model_environment'),
      HttpStore = require('diary-core/http_store'),
      Serializers = require('diary-core/model_serializers')

// Development environment

class Environment extends ModelEnvironment {
  constructor() {
    super()
    this.store = new HttpStore({root: 'http://localhost:3000/api'})
    this.serializer = new Serializers.Jsonapi()
    // Will sign in automatically, see signin page.
    this.signIn = {
      key: 'KlqsoptTHBK30IhmNSj1mjqKJjzM5BrW24xWV38WT6w=',
      email: 'bob@diary'
    }
  }
}

module.exports = Environment
