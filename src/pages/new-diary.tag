<new-diary>
    <div class="o-container--small">
        <h3 class="c-heading">Add a diary</h3>
        <form onsubmit="{save}">
            <label>
                Name
                <input ref="name" type="text" name="name" class="{c-field: true, c-field--label: true, c-field--error: model.errors.name}" required />                <diary-form-field-errors errors="{model.errors.name}"></diary-form-field-errors>
            </label>

            <label class="o-form-element c-label">
                <input ref="hideListing" name="hideListing" type="checkbox" checked="{model.hideListing}" /> Hide diary messages list?
                <div class="c-hint c-hint--static c-hint--relative">Lookup messages by keywords.</div>
            </label>

            <div class="o-form-element">
                <a href="#/">Cancel</a>
                <button class="c-button c-button--success" disabled="{saving}" type="submit">Save</button>
                <span if="{message}">{message}</span>
            </div>
        </form>
    </div>

    <script>
     import '../form-errors.tag'
     import Form from '../forms/diary-form'

     const form = new Form({view: this, store: opts.store})

     this.model = form.model

     const focus = () => {
         this.refs.name.focus()
     }

     const serialize = () => {
         this.model.name = this.refs.name.value
         this.model.hideListing = this.refs.hideListing.checked
     }

     save(e) {
         e.preventDefault()
         serialize()
         form.save()
     }

     this.on('saved', () => route('/diaries'))

     this.on('mount', () => focus())
    </script>

    <style>
     /* Fix Save button overlap. */
     :scope .c-hint.c-hint--relative {
         position: relative;
     }
    </style>
</new-diary>
