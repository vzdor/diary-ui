<diary-signin>
    <div class="c-card c-card__item">
        <h3>Sign in</h3>
        <form onsubmit="{submit}">
            <label calss="c-label">
                Email
                <input ref="email" type="email" class="{c-field: true}" required />
            </label>
            <label class="c-label">
                Password
                <input ref="password" type="password" class="{c-field: true}" required />
            </label>
            <div class="o-form-element">
                <button class="c-button c-button--success" disabled="{testing}" type="submit">Sign in</button>
                <span if="{message}">{message}</span>
            </div>
        </form>
    </div>

    <p>
        New user? <a href="#signup">Sign up.</a>
    </p>

    <script>
     import Diary from 'diary-core/diary'
     import cryptoInitializer from 'diary-core/crypto_initializer'
     import Errors from 'diary-core/errors'
     import Cookies from 'cookies.js'

     // The setTimeout's is to let the browser render the message
     // before we run heavy calculation like passwordKey().

     // this.update() will add to the event loop queue.
     // We want to add an event, but we want to be sure it will be
     // processed after the update event.

     // Shortcuts
     const store = opts.store,
           env = store.env

     if (env.signIn)
         cryptoInitializer({key: env.signIn.key}).then(async (crypto) => {
             initialize(crypto, env.signIn.email)
         })

     const afterSignin = (email) => {
         // Save email in a cookie for later use.
         Cookies.setItem('email', email)
         // Route back or to the filter page.
         route(store.routeTo || 'filter')
     }

     const initialize = async (crypto, login) => {
         env.crypto = crypto
         env.store.signIn({login, password: crypto.servicePassword})
         // Get diaries.
         try {
             const diaries = await env.model(Diary).findModels(),
                   diary = diaries[0]
             env.diary = diary
             // Instantiate diary crypto
             env.diaryCrypto = await diary.deriveCrypto()
             // Tell the App store to sign in.
             store.trigger('sign-in', {env, diaries})
             afterSignin(login)
         } catch (error) {
             // TODO:
             // 1. Clean store.signin() -- make a new store.
             // 2. Clean the password field.
             let message
             if (error instanceof Errors.UnauthorizedError)
                 message = 'Incorrect email or password.'
             else {
                 message = Errors.errorMessage(error)
                 console.error(error)
             }
             this.update({testing: false, message: message})
             focus()
         }
     }

     const initializeCrypto = (email, password) => {
         cryptoInitializer({password, email}).then((crypto) => {
             this.update({message: 'Testing the key...'})
             setTimeout(() => initialize(crypto, email))
         })
     }

     const signIn = (login, password) => {
         this.update({testing: true, message: 'Generating keys...'})
         setTimeout(() => initializeCrypto(login, password),
                    100) // XXX: Works without this in FF, does not work in Chrome.
     }

     const focus = () => {
         const email = this.refs.email.value
         if (email)
             this.refs.password.focus()
         else
             this.refs.email.focus()
     }

     const fillEmailFromCookie = () => {
         const email = Cookies.getItem('email')
         if (email) this.refs.email.value = email
     }

     submit(e) {
         e.preventDefault()
         e.preventUpdate = true
         signIn(this.refs.email.value, this.refs.password.value)
     }

     this.on('mount', () => {
         fillEmailFromCookie()
         focus()
     })
    </script>
</diary-signin>
