<diary-t-bookmark>
    <a href="{opts.model.url}">{opts.model.message}</a>
</diary-t-bookmark>

<diary-t-message>
    <!-- Avoid setAttribute('text', message), the message is long text. See riot/lib/browser/tag/update.js. If it is an object, it will not setAttribute. -->
    <diary-markup text="{new String(opts.model.message)}" tokens="{opts.model.tokens}"></diary-markup>
</diary-t-message>

<diary-results-list>
    <div if="{opts.finder.pagesCount}">
        <span class="c-text--quiet">{opts.finder.pagesCount} pages</span>
    </div>

    <div each="{model in opts.finder.models}" class="entry">
        <virtual data-is="{parent.component(model)}" model="{model}"></virtual>
        <virtual each="{name in model.labels}">
            <span class="c-badge">{name}</span>&nbsp;
        </virtual>
        <a href="#/edit/{model.id}" class="c-button c-button--ghost u-small">Edit</a>
    </div>

    <p if="{notFoundSymbol === 'not-found'}">Not found.</p>

    <p if="{notFoundSymbol === 'hide-listing'}" class="c-hint c-hint--static">
        The diary is configured to hide the list of messages from the user interface. In this mode, you lookup messages by keywords.
        <br />
    </p>

    <diary-pager finder="{opts.finder}"></diary-pager>

    <script>
     import '../markup.tag'
     import '../pager.tag'

     component(model) {
         if (model.url)
             return "diary-t-bookmark"
         else
             return "diary-t-message"
     }

     const setNotFoundSymbol = () => {
         const {models, hideListing, options} = opts.finder
         if (models.length !== 0) return
         if (hideListing && !options.text)
             this.notFoundSymbol = 'hide-listing'
         else
             this.notFoundSymbol = 'not-found'
     }

     setNotFoundSymbol()
    </script>

    <style>
     :scope .entry {
         padding-top: 0.5em;
         padding-bottom: 0.5em;
         border-bottom: 1px solid #e2e7ec;
     }
    </style>
</diary-results-list>

<not-implemented>
    <div>
        Not implemented.
    </div>

    <style>
     :scope {
         padding-top: 0.5em;
     }
    </style>
</not-implemented>

<diary-results>
    <div>
        <diary-lookup finder="{finder}"></diary-lookup>
        <div data-is="{component}" finder="{finder}"></div>
    </div>

    <script>
     // Pages
     import '../loading.tag'
     import '../lookup.tag'

     import Finder from '../finder'

     this.finder = new Finder({env: opts.store.env})

     const loading = () => {
         this.update({component: 'diary-loading'})
     }

     const loaded = () => {
         this.update({component: 'diary-results-list'})
     }

     const filter = async (label) => {
         loading()
         this.finder.capture({label})
         const models = await this.finder.find()
         if (route.query().q)
             models.forEach((model) => {
                 let score = Math.round(model.score * 100) / 100
                 model.labels = model.tokens.concat([`s: ${score}`])
             })
         loaded()
     }

     const notImplemented = (label) => {
         this.update({component: 'not-implemented'})
     }

     const router = route.create()
     router('/filter', filter)
     router('/filter/*', notImplemented)
     router('/filter/*/...', notImplemented)
     router('/filter...', filter)
     // Route using the new routes.
     route.exec()
     this.on('unmount', () => router.stop())
    </script>
</diary-results>
