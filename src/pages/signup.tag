<diary-signup>
    <div class="c-card c-card__item">
        <h3>Sign up</h3>
        <form onsubmit="{submit}">
            <label calss="c-label">
                Email
                <input ref="email" type="email" class="{c-field: true, c-field--error: model.errors.email}" value="{model.email}" required />
                <diary-form-field-errors errors="{model.errors.email}"></diary-form-field-errors>
            </label>
            <label class="c-label">
                Password
                <input ref="password" type="password" class="{c-field: true, c-field--error: model.errors.password}" value="{model.password}" required />
                <diary-form-field-errors errors="{model.errors.password}"></diary-form-field-errors>
            </label>
            <label>
                Confirm password
                <input ref="passwordConfirmation" type="password" class="{c-field: true}" value="{model.passwordConfirmation}" required />
            </label>
            <div class="o-form-element">
                <button class="c-button c-button--success" disabled="{saving}" type="submit">Sign up</button>
                <span if="{message}">{message}</span>
            </div>
        </form>
    </div>

    <p>
        <a href="#sigin">Sign in.</a>
    </p>

    <script>
     import '../form-errors.tag'
     import Form from '../forms/signup-form'

     const form = new Form({view: this, store: opts.store})

     this.model = form.model

     const serialize = () => {
         ['email', 'password', 'passwordConfirmation'].forEach((name) => {
             form.model[name] = this.refs[name].value
         })
     }

     submit(e) {
         e.preventDefault()
         serialize()
         form.save()
     }

     this.on('mount', () => {
         this.refs.email.focus()
     })
    </script>
</diary-signup>
