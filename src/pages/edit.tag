<diary-edit>
    <h3>Edit entry in {opts.store.diary.name}</h3>
    <div if="{entry}">
        <a class="c-button c-button--error" href="#" onclick="{destroy}">Delete</a> {deletingMessage}
        <entry-form entry="{entry}" store="{opts.store}" />
    </div>
    <diary-loading if="{!entry}" />

    <script>
     import '../entry-form.tag'
     import '../loading.tag'

     import Entry from 'diary-core/entry'

     // Shortcuts
     const env = opts.store.env

     const _destroy = async () => {
         this.update({deletingMessage: 'Deleting...'})
         await this.entry.destroy()
         route('/filter')
     }

     destroy(e) {
         e.preventDefault()
         if (confirm('Are you sure you want to delete the message?')) {
             _destroy()
         }
     }

     this.on('mount', async () => {
         // This can happen when visiting /edit without id.
         if (!opts.entityid) return route('/filter')
         this.entry = await env.model(Entry).find(opts.entityid)
         this.update()
     })
    </script>
</diary-edit>
