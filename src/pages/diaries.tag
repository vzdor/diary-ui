<diaries>
    <div class="o-container--small">
        <h3>Your diaries</h3>
        <ul class="c-card c-card--menu c-card--flat">
            <li each="{diary in diaries}" onclick="{diarySetter(diary)}" class="{c-card__item: true, c-card__item--active: selectedDiary === diary}">
                {diary.name}
            </li>
        </ul>
        <p>
            <a href="#/new-diary" class="c-button c-button--success"><i class="fa fa-plus"></i> New diary</a>
        </p>
    </div>

    <script>
     const store = opts.store

     this.diaries = store.diaries
     this.selectedDiary = store.diary

     const switchDiary = async (diary) => {
         if (this.selectedDiary !== diary) {
             const diaryCrypto = await diary.deriveCrypto()
             store.trigger('set-diary', diary, diaryCrypto)
         }
         route('filter')
     }

     diarySetter(diary) {
         return (e) => {
             e.preventDefault()
             e.preventUpdate = true
             switchDiary(diary)
         }
     }
    </script>

    <style>
     :scope .c-card.c-card--flat {
         box-shadow: initial;
         border-radius: initial;
     }
    </style>
</diaries>
