<diary-new>
    <h3>Add entry to {opts.store.diary.name}</h3>
    <entry-form store="{opts.store}"></entry-form>

    <script>
     import '../entry-form.tag'
    </script>
</diary-new>
