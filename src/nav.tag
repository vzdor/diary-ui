<nav-item>
    <a href="#{opts.url}" class="{c-nav__item: true, c-nav__item--active: isActive()}">
        <yield />
    </a>

    <script>
     isActive() {
         const cur = location.href.split("#")[1]
         return cur === opts.url || cur === `/${opts.url}`
     }
    </script>
</nav-item>

<diary-nav>
    <nav-item url="diaries"><i class="fa fa-home"></i> {parent.diaryName}</nav-item>
    <nav-item url="new"><i class="fa fa-plus"></i> New</nav-item>

    <nav-item url="filter">
        All <span if="{parent.opts.reminders}" class="c-badge">{parent.opts.reminders}</span>
    </nav-item>

    <nav-item each="{opts.tags}" url="filter/{name}">
	<span class="c-badge c-badge--{badge}">{name}</span>
    </nav-item>

    <nav-item url="settings">
       <i class="fa fa-edit"></i> Settings
    </nav-item>

    <script>
     const store = opts.store,
           router = route.create()

     const setDiaryName = () => {
         let name = store.diary.name
         if (name.length > 9) name = name.substring(0, 9) + '...'
         this.diaryName = name
     }

     setDiaryName()

     store.on('set-diary', () => {
         setDiaryName()
         this.update()
     })

     router(() => this.update())
     this.on('unmount', () => router.stop())
    </script>

    <style>

    </style>
</diary-nav>
