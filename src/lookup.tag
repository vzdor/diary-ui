<diary-lookup>
    <form onsubmit="{find}">
        <div class="c-input-group">
            <div class="o-field o-field--icon-right">
                <input ref="query" value="{opts.finder.options.text}" class="c-field" />
                <i class="c-icon ico ico--find ico--clickable" onclick="{find}" title="Find entries with keywords."></i>
            </div>
        </div>
    </form>

    <script>
     find(e) {
         e.preventDefault()
         let text = this.refs.query.value
         route(opts.finder.url({text: text, page: 0}))
     }

     const focus = () => {
         this.refs.query.focus()
     }

     this.on('mount', focus)
    </script>

    <style>
    </style>
</diary-lookup>
