'use strict';

const {urlQuery} = require('diary-core/urls')

function filterUrl({label, text, page} = {}) {
  let url = '/filter',
      options = {
        q: text,
        page: page
      }
  if (label) url += '/' + label
  return url + urlQuery(options)
}

module.exports = {
  filterUrl: filterUrl
}
