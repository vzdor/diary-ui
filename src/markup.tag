<diary-markup>
    <script>
     const markup = require('diary-markup/markup'),
           transformers = require('diary-markup/transformers')

     const render = () => {
         this.root.innerHTML = markup(opts.text, {
             transformers: transformers.make({stems: opts.tokens})
         })
     }

     this.on('mount', render)
     // render() -- should not be called before update.
     // To reproduce:
     // this.root.innerHTML = ' {{R from incorrect name}} instead.'
     this.on('update', render)
    </script>

    <style>
     :scope ol {
         padding-left: 1.1em;
     }
    </style>
</diary-markup>
