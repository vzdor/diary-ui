<flash>
    <div if="{message}" class="{classes}">
        <button onclick="{close}" class="c-button c-button--close">×</button>
        {message}
    </div>

    <script>
     import Model from 'diary-core/model'

     display(message, type) {
         console.log('flash')
         let classes = `c-alert c-alert--${type}`
         this.update({message: message, classes: classes})
     }

     clean() {
         this.message = null
         this.classes = null
     }

     // TODO Why here?
     /* Model.store.on('fetch-error',
              () => this.display('Connection/server error.', 'error'))

     this.parent.on('flash', this.display.bind(this))
     this.parent.on('flash-close', () => this.update())

     // Clean the message
     this.on('updated', this.clean.bind(this)) */

     close(e) {
         e.preventDefault()
         this.clean()
     }
    </script>
</flash>
