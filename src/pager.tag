<diary-pager>
    <div if="{opts.finder.pagesCount}" class="c-pagination c-pagination--diary">
        <div class="c-pagination__controls">
            <virtual each="{pages}">
                <a href="#{url}" class="{c-pagination__page: true, c-pagination__page--diary: true, c-pagination__page--current: isActive}">{page}</a>
            </virtual>
        </div>
    </div>

    <script>
     const pages = () => {
         const {options: {page}, pagesCount} = opts.finder
         if (pagesCount <= 1) return []
         // Relative page numbers.
         let nums = [-5, -2, -1, 0, 1, 2, 5]
             .map((n) => page + n)
             .filter((n) => n >= 0 && n < pagesCount)
         return nums.map((n) => {
             return {
                 url: opts.finder.url({page: n}),
                 isActive: page == n,
                 page: n + 1
             }
         })
     }
     this.pages = pages()
    </script>

    <style>
     :scope .c-pagination--diary {
         padding-top: 1.2em
     }
     :scope .c-pagination__page--diary {
         border-radius: 4px;
         padding: 0.7em;
     }
    </style>
</diary-pager>
