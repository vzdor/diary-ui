<diary-app>
    <div data-is="{layout}"  component="{component}" entityid="{entityid}" store="{store}"></div>

    <script>
     'use strict';

     import './user-layout.tag'
     import './layout.tag'

     import appStore from './app-store'

     this.store = appStore

     // collection -> user/public page -> riotjs component mapping.

     const allRoutes = {
         user: {
             default: 'filter',
             filter: 'diary-results',
             new: 'diary-new',
             edit: 'diary-edit',
             settings: 'diary-settings',
             diaries: 'diaries',
             'new-diary': 'new-diary'
         },
         public: {
             default: 'signin',
             signin: 'diary-signin',
             signup: 'diary-signup'
         }
     }

     // The workflow
     // 1. Define routes in a page component
     // 2. In the page component, call route.exec()

     // Not necessary to do that if the component handles only
     // one route.
     // The id of url is passed as entityid.

     const showComponent = (component, entityid) => {
         if (this.component == component
         ) return
         let layout = 'diary-layout'
         if (this.store.isSignedIn()) layout = 'diary-user-layout'
         this.update({component, layout, entityid})
     }

     // Save location collection in store.routeTo.
     // TODO: Write a test for this.
     const storeLocation = (collection, id) => {
         if (allRoutes.user[collection] || allRoutes.public[collection]) {
             const elements = window.location.href.split('#')
             this.store.routeTo = elements[elements.length - 1]
         }
     }

     route((collection, id) => {
         // console.log(`(App) Route ${collection} id=${id}`)
         if (! collection) return route('/filter')
         const routes = this.store.isSignedIn() ? allRoutes.user : allRoutes.public,
               comp = routes[collection]
         if (comp)
             showComponent(comp, id)
         else {
             storeLocation(collection, id)
             route(routes.default)
         }
     })
    </script>
</diary-app>
