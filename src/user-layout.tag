<diary-user-layout>
    <div class="o-grid o-panel">
        <div class="o-grid__cell--width-fixed o-panel-container" style="width: 150px;">
            <nav ref="diarynav" class="c-nav c-nav--light o-panel"></nav>
        </div>
        <div class="o-grid__cell o-panel-container">
            <div class="o-panel o-panel--page">
                <flash></flash>
                <div data-is="{opts.component}" store="{opts.store}" entityid="{opts.entityid}"></div>
            </div>
        </div>
    </div>

    <script>
     import './flash.tag'
     import './nav.tag'
     // Pages
     import './pages/settings.tag'
     import './pages/new.tag'
     import './pages/edit.tag'
     import './pages/results.tag'
     import './pages/diaries.tag'
     import './pages/new-diary.tag'

     const navOptions = {
         store: opts.store,
         reminders: 5,
         // Frequently used tags list
         tags: [
             {name: "work", badge: "success"},
             {name: "diary-project", badge: "danger"}
         ]
     }
     this.on('mount', () => {
         riot.mount(this.refs.diarynav, 'diary-nav', navOptions)
     })
    </script>

    <style>
     :scope .o-panel.o-panel--page {
         padding: 0.5em;
     }
    </style>
</diary-user-layout>
