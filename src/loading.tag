<diary-loading>
    <div>
        Loading, please wait...
    </div>

    <style>
     :scope {
         padding-top: 1em;
     }
    </style>
</diary-loading>
