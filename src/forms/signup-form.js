'use strict';

const Form = require('../form'),
      Registration = require('diary-core/registration'),
      cryptoInitialize = require('diary-core/crypto_initializer')

class SignUpForm extends Form {
  constructor({view, store}) {
    const env = store.env.clone()
    super(view, env.newModel(Registration))
    this.env = env
    this.store = store
  }

  validate() {
    const errors = {}
    if (this.model.password !== this.model.passwordConfirmation)
      errors.password = ['does not match confirmation']
    this.model.errors = errors
  }

  async save() {
    const initializeAndSave = async (resolve) => {
      this.env.crypto = await cryptoInitialize({
        password: this.model.password,
        salt: this.model.email
      })
      await super.save({validate: false})
      resolve()
    }
    const queue = (fn) => {
      return new Promise((resolve) => {
        setTimeout(() => fn(resolve), 100)
      })
    }
    this.validate()
    if (this.anyErrors()) return

    this.view.update({saving: true, message: 'Generating keys..'})
    // We should be able to wait for the form.save().
    // view.update() will not render instantly, but will add a message
    // to the event loop queue.
    // queue(...) will delay our function, so view.upadte() will be
    // processed first.
    return queue(initializeAndSave)
  }

  async saved() {
    const env = this.env,
          diaries = this.model.diaries,
          diary = diaries[0]
    // Sign in to the model store.
    env.store.signIn({
      login: this.model.email,
      password: env.crypto.servicePassword
    })
    // Set current diary and diary messages crypto.
    env.diary = diary
    env.diaryCrypto = await diary.deriveCrypto()
    // Set store env and everything else.
    this.store.trigger('sign-in', {env, diaries})
  }
}

module.exports = SignUpForm
