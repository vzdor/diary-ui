'use strict';

const Form = require('../form.js'),
      Diary = require('diary-core/diary')

class DiaryForm extends Form {
  constructor({view, model, store}) {
    super(view, model || store.env.newModel(Diary))
    this.store = store
  }

  validate() {
    this.model.errors = {} // Clean errors.

    const errors = this.model.errors, // Shortcuts
          name = this.model.name

    const exists = this.store.diaries.find((diary) => diary.name == name)
    if (exists) errors.name = ['Diary with that name exists.']
  }

  saved() {
    this.store.trigger('add-diary', this.model)
    super.saved()
  }
}

module.exports = DiaryForm
