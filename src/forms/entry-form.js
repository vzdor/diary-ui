'use strict';

const Form = require('../form'),
      Indexer = require('diary-core/zindexer'),
      Entry = require('diary-core/entry')

class EntryForm extends Form {
  constructor({view, model, store}) {
    super(view, model || store.env.newModel(Entry))
    this.store = store
  }

  saveModel() {
    const indexer = new Indexer({env: this.store.env})
    return this.model.save(indexer)
  }
}

module.exports = EntryForm
