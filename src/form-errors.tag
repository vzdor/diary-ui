<diary-form-field-errors>
    <ul if="{opts.errors}" class="c-list c-list--unstyled">
        <li each="{error in opts.errors}">
            {error}
        </li>
    </ul>

    <style>
     :scope ul li {
         padding: 0 0.5em;
     }
    </style>
</diary-form-field-errors>
