'use strict';

const path = require("path"),
      webpack = require("webpack")

const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = function(env, args) {
  env = env || {}

  const options = {
    entry: {
      app: './src/app.js',
      test: './test/test.js',
      'signup-test': './test/signup-test.js'
    },
    // resolve: {
    //   alias: { // Re-use stemtiny from diary-ui, instead of including twice
    //     stemtiny: path.resolve('./node_modules/stemtiny'),
    //   },
    // },
    output: {
      path: path.resolve(__dirname, './dist'),
      filename: '[name].js'
    },
    // https://webpack.github.io/docs/configuration.html#node
    node: {
      Buffer: false, // do not add Buffer polyfill.
      // I use Buffer in diary-core/base64, but only in nodejs. Webpack adds a polyfill Buffer is used.
      global: false,
      crypto: false
    },
    externals: [
      {
        crypto: true // Do not try to `require('crypto')`
      }
    ],
    // Add sourcemaps. There is an option to make the sourcemaps file smaller
    devtool: "source-map",
    module: {
      // Loaders. They translate/compile the files.
      rules: [
        // The riotjs loader with working sourcemaps
        {
          test: /\.tag$/,
          // In which directory to look for the files?
          include: path.resolve(__dirname, "src"),
          // Loader library. install with: npm install --save-dev libname
          use: 'riot-tag-loader',
        },
        {
          test: /\.css$/,
          include: path.resolve(__dirname, "src"),
          use: ExtractTextPlugin.extract({
            // fallback: "style-loader",
            use: "css-loader?-url" // Do not look for url(file) files.
          })
        }
      ]
    },
    plugins: [
      // Save css bundle to a file.
      new ExtractTextPlugin("app.css"),

      // We have a few entry points. Save common modules for all of them
      // to common.js.
      new webpack.optimize.CommonsChunkPlugin({
	name: 'common',
        // All chunks. A chunk is an entry point.
	// chunks: ['app', 'test'],
      }),
    ]
  }

  if (env.modelEnv) {
    // https://webpack.js.org/plugins/normal-module-replacement-plugin/

    // Substitute environment file with specified file.
    const plugin = new webpack.NormalModuleReplacementPlugin(/src\/environment\.js/, `./environments/${env.modelEnv}-environment.js`)
    options.plugins.unshift(plugin)
  }

  return options
}
