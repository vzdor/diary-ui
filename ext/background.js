function openDiary() {
  chrome.tabs.create({
    'url': '/diary.html'
  })
}

/*
  Add openDiary() as a listener to clicks on the browser action.
*/
chrome.browserAction.onClicked.addListener(openDiary)
