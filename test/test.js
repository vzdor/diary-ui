'use strict';

// The default, wait-by-observer does not work with `ul li:last-child`
// selector.
const waitForElement = require('wait-for-element/lib/wait-by-timer')
      // WebConcole = require('web-console-reporter')  -- is not a module.

const appStore = require('../src/app-store'),
      modelStore = appStore.env.store

// This works in browser because webpack loads browser assert package.
const assert = require('assert')

async function addDiary({name = 'New diary', hideListing = false} = {}) {
  const formSelector = '[data-is="new-diary"] form ',
        nameInput = await waitForElement(formSelector + '[ref="name"]'),
        listingInput = document.querySelector(formSelector + "[ref='hideListing']"),
        button = document.querySelector(formSelector + 'button')
  nameInput.value = name
  listingInput.checked = hideListing

  button.click()
}

async function addEntry({message = 'Hello.'} = {}) {
  const input = await waitForElement('[ref="message"]'),
        button = document.querySelector('form button')
  input.value = message
  button.click()
}

describe('Signin', function() {
  it('renders signin page', async function() {
    route('/filter')
    const password = await waitForElement('[ref="password"]')
    assert(password)
  })

  it('signs in and redirects to /filter', function(done) {
    const password = document.querySelector('[ref="password"]'),
          email = document.querySelector('[ref="email"]'),
          button = document.querySelector('form button')

    email.value = 'bob@diary'
    password.value = 'test'

    this.timeout(6000)
    // Wait for the route change.
    const router = route.create()
    router('/filter', () => {
      router.stop()
      done()
    })
    button.click()
  })
})

describe('New entry', function() {
  beforeEach(function() {
    route('/new')
  })

  it('renders new page', async function() {
    await waitForElement('[ref="message"]')
  })

  context('clicking Save button', function() {
    it('adds a new message', async function() {
      await addEntry({message: 'Hello world.'})
      const ele = await waitForElement('.entry'),
            text = ele.textContent.trim()
      assert(text.includes('Hello world.'), `${text} does't include "Hello world."`)
    })
  })
})

describe('Edit entry', function() {
  beforeEach(async function() {
    modelStore.clear('entries')
    route('/new')
    await addEntry({message: 'Hello world.'})
    // Wait for the filter page.
    await waitForElement('.entry')
  })

  it('renders edit page after clicking the Edit button', async function() {
    const edit = await waitForElement('[href="#/edit/1"]')
    edit.click()
    const message = await waitForElement('form [ref="message"]')
    assert.equal(message.value, 'Hello world.')
  })

  it('renders filter page and shows updated message', async function() {
    route('/edit/1')
    const button = await waitForElement('form button[type="submit"]'),
          message = document.querySelector('form [ref="message"]')
    message.value = 'Hello.'
    button.click()
    const entry = await waitForElement('.entry'),
          text = entry.textContent.trim()
    assert(text.includes('Hello.'), `${text} does'nt include "Hello."`)
  })
})

describe('Diaries page', function() {
  it('renders diaries', async function() {
    route('/diaries')
    await waitForElement('[data-is="diaries"]')
  })
})

describe('New diary', function() {
  beforeEach(function() {
    route('/new-diary')
  })

  it('renders New diary page', async function() {
     await waitForElement('[ref="name"]')
  })

  context('clicking Save button', function() {
    it('adds a diary', async function() {
      await addDiary()
      const el = await waitForElement('[data-is="diaries"] ul li:last-child')
      assert.equal(el.textContent.trim(), 'New diary')
    })

    it('shows an erorr if diary exists', async function() {
      await addDiary()
      const formSelector = '[data-is="new-diary"] form '
      await waitForElement(formSelector + '[ref="name"].c-field--error')
    })
  })

  context('adding Hide listing diary', function() {
    it('adds diary', async function() {
      addDiary({name: 'Secrets', hideListing: true})
      const el = await waitForElement('[data-is="diaries"] ul li:last-child')
      // How do we test what diary type was added?
      el.click()
      const message = await waitForElement('[data-is="diary-results-list"] p'),
            text = message.textContent.trim()
      assert(/diary is configured to hide the list of messages/.test(text))
    })
  })
})

describe('Diary with hideListing', function() {
  before(async function() {
    // Add diary and switch to it.
    route('/new-diary')
    await addDiary({name: 'New secrets', hideListing: true})
    const el = await waitForElement('[data-is="diaries"] ul li:last-child')
    el.click()
  })

  beforeEach(function() {
    modelStore.clear('entries')
  })

  async function add() {
    route('/new')
    addEntry()
    // Wait for results page.
    await waitForElement('[data-is="diary-results-list"')
  }

  it('adds entry but shows blank /filter page', async function() {
    await add()
    // Test that the entry was added to the store,
    assert.equal(Object.keys(modelStore.pages.entries).length, 1)
    // but is not displayed
    assert(!document.querySelector('.entry'))
  })

  it('shows the entry on keyword lookup', async function() {
    await add()
    // A reference to the added entry.
    const entry = modelStore.pages.entries['1']

    // Add zindexer table.
    modelStore.pages.zqueries = {
      [entry.localId] : {
        id: entry.localId,
        tokens: [true]
      }
    }
    // Type into the keywords field and hit enter
    const selector = '[data-is="diary-results"] ',
          form = await waitForElement(selector + 'form'),
          input = document.querySelector(selector + '[ref="query"]')
    input.value = 'Hello'

    // TODO: the submit() does not work. May be it is related to
    // how riot <form onsubmit={func} is implemented.
    // See lookup.tag.

    // form.submit()

    const button = document.querySelector(selector + 'form i')
    button.click()
    const entryEl = await waitForElement('.entry')
  })
})
