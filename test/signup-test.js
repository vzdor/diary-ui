'use strict';

const waitForElement = require('wait-for-element/lib/wait-by-timer')

const appStore = require('../src/app-store'),
      modelStore = appStore.env.store

const assert = require('assert')

describe('Signup', function() {
  before(function() {
    delete appStore.env.diary
    delete appStore.env.diaryCrypto
    delete appStore.env.crypto
    modelStore.clear()
  })

  it('renders signup page', async function() {
    route('/signup')
    const password = await waitForElement('[ref="password"]')
    assert(password)
  })

  it('signs up and redirects to /filter', function(done) {
    const password = document.querySelector('[ref="password"]'),
          passwordConfirmation = document.querySelector('[ref="passwordConfirmation"]'),
          email = document.querySelector('[ref="email"]'),
          button = document.querySelector('form button')

    email.value = 'bob@diary'
    password.value = 'test'
    passwordConfirmation.value = password.value

    this.timeout(6000)
    // Wait for the route change.
    const router = route.create()
    router('/filter', () => {
      router.stop()
      assert(appStore.isSignedIn())
      done()
    })
    button.click()
  })
})
